import React, {useState,useEffect} from 'react'

export default function CreateShoeHook() {
    let [bins, setBins] = useState([])

    const [shoes, setShoes] = useState({
        name: "",
        manufacturer: "",
        color: "",
        picture_url: "",
        bin: "",
    })

    useEffect(() => {
        if (bins.length === 0) {
            fetch(`http://localhost:8100/api/bins/`)
                .then(res => res.json())
                .then(res => setBins(res.bins))
        }
    },[bins])

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {...shoes}
        const shoeUrl = `http://localhost:8080/bins/${shoes.bin}/shoes/`

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(shoeUrl, fetchConfig);

        if (response.ok) {
            const newShoe = await response.json();

            setShoes({
                name: "",
                manufacturer: "",
                color: "",
                picture_url: "",
                bin: ""
            })
        }
    }

    return (
        <>
            <h1>Create a shoe</h1>
            <form onSubmit={handleSubmit} id="create-shoe-form">
                <div className="form-floating mb-3">
                    <input value={shoes.name} onChange={(event) => setShoes({ ...shoes, name: event.target.value })} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                    <label htmlFor="name">Shoe name</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={shoes.manufacturer} onChange={(event) => setShoes({ ...shoes, manufacturer: event.target.value })}placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                    <label htmlFor="manufacturer">Manufacturer</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={shoes.color} onChange={(event) => setShoes({ ...shoes, color: event.target.value })} placeholder="color" required type="text" name="color" id="color" className="form-control" />
                    <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={shoes.picture_url} onChange={(event) => setShoes({ ...shoes, picture_url: event.target.value })}placeholder="Picture_url" required type="text" name="picture_url" id="picture_url" className="form-control" />
                    <label htmlFor="title">Picture Url</label>
                </div>
                <div className="mb-3">
                    <select value={bins.id} onChange={(event) => setShoes({ ...shoes, bin: event.target.value })} required name="bin" id="bin" className="form-select">
                        <option value="">Choose a bin</option>
                        {bins.map(bin => {
                            return(
                                <option key={bin.id} value={bin.id}>
                                    {bin.closet_name}
                                </option>
                            );
                        })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
            <div className="alert alert-success d-none mb-0" id="success-message">
                Congratulations! New Shoe Created!
            </div>
        </>
    )
}
