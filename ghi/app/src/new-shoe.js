window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8100/api/bins/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
          return `
              <div class="alert alert-danger" role="alert">Something went wrong!
              </div>
          `;
        } else {
        const data = await response.json();
        const selectTag = document.getElementById('bin');

        for (let bin of data.bins) {
            const option = document.createElement('option');
            option.value = bin.id;
            option.innerHTML = bin.name;
            selectTag.appendChild(option);
        }
        // State fetching code, here...
        const formTag = document.getElementById('create-shoe-form');
        formTag.addEventListener('submit', async event => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));
            console.log(json)
            const shoeUrl = 'http://localhost:8000/api/shoes/';
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(shoeUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const newShoe = await response.json();
            }
        });
      }
    } catch (e) {
        return `
          <div class="alert alert-danger" role="alert">Something went wrong!
          </div>
        `;
      }
  });
