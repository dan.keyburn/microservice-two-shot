import {useState, useEffect} from 'react';
import { Link } from 'react-router-dom';
import HatsForm from './HatsForm';

function HatsList(props) {
    const [hats, setHats] = useState([])


    const getData = async ()=> {
        const resp = await fetch('http://localhost:8090/api/hats/')
        const data = await resp.json()

        setHats(data)
        }

        const handleDelete = async (id) => {
            const resp = await fetch(`http://localhost:8090/api/hats/${id}/`, { method:"DELETE"})
            const data = await resp.json()
            getData();
        }

        useEffect(() => {
            getData();
        }, [])

console.log(props.hats)
return (
    <>
    <table className="table table-striped">
      <thead>
        <tr>
        <th>Hats</th>
        <th>Fabric</th>
        <th>Color</th>
        <th>Location</th>
        <th>Style</th>
        <th>URL</th>
        <th>delete</th>
        </tr>
      </thead>
      <tbody>
          {props.hats?.map(hat => {
              return (
                  <tr key={hat.id}>
                      <td>{ hat.id }</td>
                      <td>{ hat.fabric }</td>
                      <td>{ hat.color }</td>
                      <td>{ hat.location }</td>
                      <td>{ hat.style_name }</td>
                      <td><div>
                    <img src={hat.picture_url} alt="picture_url" style={{ width: 100, height: 100 }} />
                  </div>
                    </td>
                      <td>
                        <button onClick={()=> handleDelete(hat.id)}>Delete</button>
                    </td>
                  </tr>
              );
          })}
      </tbody>
    </table>
    <a href="http://localhost:3000/hats/new/"><button>ADD A NEW HAT</button> </a>
    </>
);
}

export default HatsList;
