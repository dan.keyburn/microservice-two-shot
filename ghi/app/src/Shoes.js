function ShoesList(props) {
    if (props.shoes === undefined) {
        return null;
    }

    return (
        <>
            <table className="table table-striped">
                <thead>
                <tr>
                <th>Shoes</th>
                <th>Bin</th>
                </tr>
                </thead>
                <tbody>
                    {props.shoes.map(shoe => {
                        return (
                            <tr key={ shoe.id }>
                                <td>{ shoe.name }</td>
                                <td>{ shoe.bin }</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            <button>Add a shoe</button>
        </>
    );
}

export default ShoesList;
