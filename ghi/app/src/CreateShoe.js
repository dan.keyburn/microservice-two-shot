import React from 'react';

class ShoeForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: "",
            manufacturer: "",
            color: "",
            picture_url: "",
            bin: "",
            bins: []
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleNameChange = this.handleNameChange(this);
        this.handleManufacturerChange = this.handleManufacturerChange(this);
        this.handleColorChange = this.handleColorChange(this);
        this.handlePictureUrlChange = this.handlePictureUrlChange(this);
        this.handleBinChange = this.handleBinChange(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        console.log(data);
        delete data.bins;

        const shoeUrl = `http://localhost:8080/bins/${data.bin}/shoes/`;  // Object containing shoes from bin {data.bin}
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe);

            const cleared = {
                name: "",
                manufacturer: "",
                color: "",
                picture_url: "",
                bin: ""
            }
            this.setState(cleared);
        }
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value})
    }

    handleManufacturerChange(event) {
        const value = event.target.value;
        this.setState({manufacturer: value})
    }

    handleColorChange(event) {
        const value = event.target.value;
        this.setState({color: value})
    }

    handlePictureUrlChange(event) {
        const value = event.target.value;
        this.setState({picture_url: value})
    }

    handleBinChange(event) {
        const value = event.target.value;
        this.setState({bin: value})
    }

    async componentDidMount() {
        const url = `http://localhost:8100/api/bins/`;

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({bins: data.bins});
        }
    }



    render() {
        return (
            <>
                <h1>Create a shoe</h1>
                <form onSubmit={this.handleSubmit} id="create-shoe-form">
                    <div className="form-floating mb-3">
                        <input value={this.state.name} onChange={this.handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                        <label htmlFor="name">Shoe name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={this.state.manufacturer} onChange={this.handleManufacturerChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                        <label htmlFor="manufacturer">Manufacturer</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={this.state.color} onChange={this.handleColorChange} placeholder="color" required type="text" name="color" id="color" className="form-control" />
                        <label htmlFor="color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={this.state.picture_url} onChange={this.handlePictureUrlChange} placeholder="Picture_url" required type="text" name="picture_url" id="picture_url" className="form-control" />
                        <label htmlFor="title">Picture Url</label>
                    </div>
                    <div className="mb-3">
                        <select value={this.state.bin} onChange={this.handleBinChange} required name="bin" id="bin" className="form-select">
                            <option value="">Choose a bin</option>
                            {this.state.bins.map(bin => {
                                return(
                                    <option key={bin.id} value={bin.id}>
                                        {bin.closet_name}
                                    </option>
                                );
                            })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
                <div className="alert alert-success d-none mb-0" id="success-message">
                    Congratulations! New Shoe Created!
                </div>
            </>
        )
    }
}

export default ShoeForm;
