import Nav from './Nav';

import HatsForm from './HatsForm';
import MainPage from './MainPage';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import HatsList from './HatsList';
import ShoeList from './Shoes';
import CreateShoeHook from './CreateShoeHook';

function App(props) {
  if (props.hats === undefined) {
    return null;
  }

return (
  <BrowserRouter>
    <Nav />
    <div className="container">
    <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats/new" element={<HatsForm hats={props.hats}/>} />
          <Route path="hats" element={<HatsList hats={props.hats} />} />
          <Route path="/shoes/" element={<ShoeList shoes={props.shoes}/>} />
          <Route path="/shoes/create" element={<CreateShoeHook />} />
    </Routes>
    </div>
  </BrowserRouter>
);


}

export default App;
