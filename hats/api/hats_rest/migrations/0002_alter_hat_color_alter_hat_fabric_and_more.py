# Generated by Django 4.0.3 on 2022-12-03 17:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hat',
            name='color',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='hat',
            name='fabric',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='hat',
            name='picture_url',
            field=models.URLField(null=True),
        ),
        migrations.AlterField(
            model_name='hat',
            name='style_name',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='locationvo',
            name='import_href',
            field=models.CharField(max_length=200, unique=True),
        ),
    ]
