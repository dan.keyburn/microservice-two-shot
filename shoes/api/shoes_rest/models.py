from django.db import models
from django.urls import reverse
from django.core.exceptions import ObjectDoesNotExist

# Create your models here.
class BinVO(models.Model):
    closet_name = models.CharField(max_length=200)
    bin_size = models.PositiveSmallIntegerField(default=1)
    bin_number = models.PositiveSmallIntegerField(default=1)

    def __str__(self):
        return f"{self.closet_name}, {self.id}"

class Shoe(models.Model):
    name = models.CharField(max_length=200)
    manufacturer = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField(max_length=300)

    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name
