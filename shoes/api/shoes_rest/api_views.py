from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Shoe, BinVO

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["closet_name", "bin_number", "bin_size"]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["name", "closet_name"]

    # def get_extra_data(self, o):
    #     return {"bin": o.bin.closet_name}


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "name",
        "manufacturer",
        "color",
        "picture_url",
        "bin"
    ]
    encoders = {
        "bin": BinVODetailEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    """
    Lists the shoes by name and the link to the shoe
    for the specified bin id.

    Returns a dictionary with a single key "shoes" which
    is a list of shoe names and URLS. Each entry in the list
    is a dictionary that contains the name of the shoe and
    the link to the shoe's information.

    {
        "shoes": [
            {
                "name": shoe's name,
                "href": URL to the shoe,
            },
            ...
        ]
    }
    """

    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoe.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
